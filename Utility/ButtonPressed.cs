﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
 
public class ButtonPressed : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

	[HideInInspector]
	public bool buttonPressed;

	public void OnPointerDown(PointerEventData eventData)
	{
		buttonPressed = true;
	}
 
	public void OnPointerUp(PointerEventData eventData)
	{
		buttonPressed = false;
	}
	
	public void OnPointerExit(PointerEventData eventData)
	{
		buttonPressed = false;
	}
}