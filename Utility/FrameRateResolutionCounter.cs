﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameRateResolutionCounter : MonoBehaviour {

	public Text resolutionDisplay;
	public Text framerateDisplay;
	[Tooltip("Counts frames per interval and calculates a per second amount! Use with caution and not with short intervals (<1s)!")]
	public bool average = false;
	public float interval = 1.0f;
	
	private float timeCounter = 0;
	private int frameCounter = 0;
	
	
	void Update()
	{
		resolutionDisplay.text = Screen.width + "x" + Screen.height;
		timeCounter += Time.deltaTime;
		frameCounter++;
		if(timeCounter >= interval && interval > 0)
		{
			if(average)
				framerateDisplay.text = (int)((float)frameCounter / interval) + " FPS";
			else
				framerateDisplay.text = (int)(1/Time.deltaTime) + " FPS";
			resolutionDisplay.text = Screen.width + "x" + Screen.height;
			timeCounter = 0;
			frameCounter = 0;
		}
	}
	
}
