﻿/*
 * Your Spin me Right Round Baby, Right Round!
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    //Drehgeschwindigkeit
	public Vector3 speed;
	
    void Update()
    {
        transform.Rotate(speed * Time.deltaTime);
    }
}
