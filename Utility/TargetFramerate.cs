﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFramerate : MonoBehaviour {

	[SerializeField]
	private int fps = 60; //readonly or set in editor
	
	void Awake()
	{
		QualitySettings.vSyncCount = 0;  // VSync must be disabled
		Application.targetFrameRate = fps;
	}
	
	void UpdateTargetFramerate(int newFps)
	{
		Application.targetFrameRate = newFps;
		fps = newFps; // only visual indicator in editor or if anything wants to read the value;
	}
}
