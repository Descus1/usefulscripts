﻿/*
 * Das Object mit diesem Script folgt einem Transform welches in Unity übergeben wird.
 * Achtung: nicht das ganze game Object sondern nur der Transform Component
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
	//Geschwindigkeit mit der das Objekt folgen soll (1 = Folgt nicht, 0 = 1 Sek bis zur Aktuellen position)
	public float smoothness = 0.95f;
	//Soll die maximale distanz verwendet werden?
	public bool clampOffset = true;
	//Maximale distanz zum Ziel
	public float maximumOffset = 3.0f;
	//Transorm zum Übergeben
	public Transform target;
	
    void LateUpdate()
    {
		float lerpFactor = (1-smoothness)*Time.deltaTime;
        Vector3 lerped = Vector3.Lerp(transform.position, target.position, lerpFactor);
		
		if(Vector3.Distance(lerped, target.position)> maximumOffset && clampOffset)
		{
			transform.position = target.position + (lerped-target.position).normalized * maximumOffset;
		}
		else
			transform.position = lerped;
    }
}
